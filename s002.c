#include <stdio.h>
#include <math.h>

/*
 * gcc s002.c -lm -o s002.out
 */

const double sqrt5 =  pow(5, 0.5);
const double phi = (1 + sqrt5) / 2;

 /*
  * Predicate function that returns 0
  * if the argument is even, 1 otherwise.
  */
int is_even(int n)
{
    return n % 2 == 0 ? 0 : 1;
}

/*
 * Sum all the elements of the Fibonacci sequence
 * in intervall [0, upper_limit] that satisfy the predicate.
 */
int sum_upto(int upper_limit, int (*predicate) (int))
{
    int a = 0;
    int b = 1;    
    int sum = 0;    
    while (a <= upper_limit) {
        if (predicate(a) == 0) {
            sum += a;
        }
       int tmp = b;
        b += a;
        a = tmp;
     }
    
    return sum;
}

/*
 * Calculate the nth element of the Fibonacci seqeuence
 * using the golden ratio.
 */
int fib_golden_ratio(int n)
{
    if (n < 0) {
        return 0;
    }
    double res_d = (pow(phi, n) - pow(-phi, -n)) / sqrt5;
    int res = res_d; 
    return res;
}

/*
 * Sum all the elements of the Fibonacci sequence
 * in intervall [0, upper_limit] that satisfy the predicate.
 *
 * The elements are calculated using the golden ratio.
 */
int sum_upto_with_golden_ratio(int upper_limit, int (*predicate) (int))
{    
    int sum = 0;
    int n = 0;
    int fib_nth = 0;
     while (fib_nth <= upper_limit) {
        if (predicate(fib_nth) == 0) {
            sum += fib_nth;
        }
        fib_nth = fib_golden_ratio(n);
        ++n;        
    }

    return sum;
}

void test()
{
    /*
        0 1 1 2 3 5 8 13 21
    */
    int pairs[10][2] = { {0, 0}, {1, 0}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {8, 10}, {9, 10} };
    int i;
    for (i = 0; i < 10; ++i) {
        int res = sum_upto(pairs[i][0], is_even);
        if (res != pairs[i][1]) {
            printf("Test of sum_upto(%d, is_even) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);            
           return;
        }
    }

    for (i = 0; i < 10; ++i) {
        int res = sum_upto_with_golden_ratio(pairs[i][0], is_even);
        if (res != pairs[i][1]) {
            printf("Test of sum_upto_with_golden_ratio(%d, is_even) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);            
           return;
        }
    }
    
    printf("All tests passed.\n");
}

int main(void)
{
/*
    test();
*/

    int upper_limit = 4 * 1000 * 1000;
/*
    printf("sum=%d\n", sum_upto_with_golden_ratio(upper_limit, is_even));
*/
    printf("sum=%d\n", sum_upto(upper_limit, is_even));

    return 0;
}
