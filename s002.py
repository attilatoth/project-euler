#!/usr/bin/env python

import unittest


def is_even(n):
    """Predicate to be used as a condition.
    """
    return n % 2 == 0


def sum_upto_with_golden_ratio(upper_limit, pred):
    """Calculate the sum of the elements
    of the Fibonacci sequence up to the element
    that is less than equal to upper_limit.

    The Fibonacci sequence is created using the golden ratio.
    """

    SQRT_5 = 5 ** 0.5
    PHI = (1 + SQRT_5) / 2

    def fib_golden_ratio(n):
        return 0 if n < 0 else int((PHI ** n - (-PHI) ** -n) / SQRT_5)

    sum, n, fib_nth = 0, 0, 0
    while fib_nth <= upper_limit:
        if pred(fib_nth):
            sum += fib_nth
        fib_nth = fib_golden_ratio(n)
        n += 1

    return sum


def sum_upto_with_generator(upper_limit, pred):
    """Calculate the sum of the elements
    of the Fibonacci sequence up to the element
    that is less than equal to upper_limit.

    The Fibonacci sequence is created by a generator.
    """
    def fib_gen():
        a, b = 0, 1
        while True:
            yield a
            a, b = b, a + b
    s = 0
    for f in fib_gen():
        if f > upper_limit:
            break
        elif pred(f):
            s += f
    return s


def sum_upto_iteration(n, is_even):
    """Calculate the sum of the elements
    of the Fibonacci sequence up to the element
    that is less than equal to upper_limit.

    The Fibonacci sequence is created on the fly.
    """
    a, b, sum = 0, 1, 0
    while b <= n:
        a, b, sum = b, a + b, b + sum if is_even(b) else sum
    return sum


class SumEvenFibsTest(unittest.TestCase):

    def setUp(self):
        self.pairs = [[0, 0], [1, 0], [2, 2], [
            3, 2], [4, 2], [5, 2], [8, 10], [9, 10]]

    def test(self):
        for pair in self.pairs:
            res = sum_upto_iteration(pair[0], is_even)
            self.assertEqual(
                pair[1], res, "sum_upto_iteration({}, is_even) should be {} but was {}.".format(pair[0], pair[1], res))

            res = sum_upto_with_generator(pair[0], is_even)
            self.assertEqual(
                pair[1], res, "sum_upto_with_generator({}, is_even) should be {} but was {}.".format(pair[0], pair[1], res))

            res = sum_upto_iteration(pair[0], is_even)
            self.assertEqual(
                pair[1], res, "sum_upto_iteration({}, is_even) should be {} but was {}.".format(pair[0], pair[1], res))


if __name__ == '__main__':

    # unittest.main()

    upper_limit = 4 * 1000 * 1000

    # print "sum={}".format(sum_upto_iteration(upper_limit, is_even))
    # print "sum={}".format(sum_upto_with_generator(upper_limit, is_even))
    print "sum={}".format(sum_upto_with_golden_ratio(upper_limit, is_even))
