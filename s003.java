public class s003 {

    /**
     * Calculate the largest prime factor of n.
     * Return 1 if n is prime.
     *
     * @param n the number to get the largest prime factor of
     * @return the largest factor
     */
    private static int largestPrimeFactor(long n) {
        int result = 1;
        long copyOfN = n;
        for (int i = 2; i <= n; ++i) {
            if (n % i == 0) {
                result = i;
                n /= i;
                i--;
            }
        }
        return result == copyOfN ? 1 : result;
    }

    private static void test() {
        int[][] pairs = {
            {-5, 1}, {-4, 1}, {-3, 1}, {-2, 1}, {-1, 1}, {-0, 1}, {1, 1}, {2, 1},
            {3, 1}, {4, 2}, {5, 1}, {6, 3}, {7, 1}, {8, 2}, {9, 3}, {10, 5},
        };
        for (int i = 0; i < pairs.length; ++i) {
            int res = largestPrimeFactor(pairs[i][0]);
            if (pairs[i][1] != res) {
                System.out.printf("Test of largestPrimeFactor(%d) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);
                return;
            }            
        }
        System.out.println("All tests passed.");
    }
    
    public static void main(String[] args) {

        // test();

        long n = 600851475143L;
        System.out.println(largestPrimeFactor(n));
    }
}
