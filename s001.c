#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

/*
 * Calculate the sum of integers in [1, upper_limit) that are multiples
 * of any of the integers given via varargs.
 * 
 * n is the length of varargs.
 */
long calc_sum_of_multiples(int upper_limit, int n, ...)
{
    if (upper_limit < 0 || n < 0 || upper_limit < n) {
        return 0L;
    }

    long sum = 0L;
    int i;
    for (i = 1; i < upper_limit; ++i) {
        va_list args;
        va_start(args, n);
        int j = 0;
        int is_multiple = 0;
        while (j < n && !is_multiple) {
            is_multiple |= (i % va_arg(args, int) == 0);
            ++j;
        }
        va_end(args);
        if (is_multiple) {
            sum += i;
        }
    }
    return sum;
}

void test()
{
    long res1 = calc_sum_of_multiples(0, 2, 2, 3);
    assert(res1 == 0 && "Should be 0");
    
    res1 = calc_sum_of_multiples(2, 2, 2, 3);
    assert(res1 == 0 && "Should be 0");

    res1 = calc_sum_of_multiples(9, 2, 2, 3);
    assert(res1 == 23 && "Should be 32");

    res1 = calc_sum_of_multiples(10, 2, 2, 3);
    assert(res1 == 32 && "Should be 32");

    res1 = calc_sum_of_multiples(11, 2, 2, 3);
    assert(res1 == 42 && "Should be 42");

    res1 = calc_sum_of_multiples(12, 2, 2, 3);
    assert(res1 == 42 && "Should be 42");

    res1 = calc_sum_of_multiples(13, 2, 2, 3);
    assert(res1 == 54 && "Should be 54");

    printf("All tests passed.\n");
}

int main(void)
{
/*
    test();
*/

    long sum = calc_sum_of_multiples(1000, 2, 3, 5);
    printf("sum=%ld\n", sum);

    return 0;
}
