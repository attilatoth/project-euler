#!/usr/bin/env python

import unittest


def calc_sum_of_multiples(upper_limit, *conds):
    """Calculate the sum of integers in [1, upper_limit)
    that are multiples of any of the integers given in conds.
    """
    sum = 0
    for x in range(1, upper_limit):
        is_multiple = False
        for cond in conds:
            is_multiple |= x % cond == 0
            if is_multiple:
                break
        if is_multiple:
            sum += x
    return sum


def calc_sum_of_multiples_other(upper_limit, a, b):
    """Add the sum of multiples of a to the sum of multiples of b
    then subtract the sum of multiples of (a * b) that was added twice
    """
    return sum(range(0, upper_limit, a)) + sum(range(0, upper_limit, b)) - sum(range(0, upper_limit, a * b))


class TestSumOfMultiplesClass(unittest.TestCase):

    def setUp(self):
        self.ints = [2, 3]
        self.pairs = [[0, 0], [2, 0], [9, 23], [
            10, 32], [11, 42], [12, 42], [13, 54]]

    def test(self):
        for pair in self.pairs:
            res = calc_sum_of_multiples(pair[0], *self.ints)
            self.assertEqual(
                pair[1], res, "calc_sum_of_multiples({}, {}) should be {} but was {}.".format(pair[0], self.ints, pair[1], res))
            res = calc_sum_of_multiples_other(pair[0], *self.ints)
            self.assertEqual(
                pair[1], res, "calc_sum_of_multiples({}, {}) should be {} but was {}.".format(pair[0], self.ints, pair[1], res))


def main():
    print "sum={}".format(calc_sum_of_multiples(1000, 3, 5))
    # print "sum={}".format(calc_sum_of_multiples_other(1000, 3, 5))

    # a tad more pythonic one
    # sum = sum([x for x in range(3, 1000) if x % 3 == 0 or x % 5 == 0])
    # print "sum={}".format(sum)

if __name__ == '__main__':

    # unittest.main()

    main()
