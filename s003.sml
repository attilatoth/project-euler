(*
 * Calculate the largest prime factor of n.
 * Return 1 if n is prime.
 *)
fun largest_prime_factor n : Int64.int =
    let
	fun aux n acc = 
	    if n <= 1 then acc
	    else if n mod acc = 0 then aux (n div acc) acc
	    else aux n (acc + 1)
		     
	val res = aux n 2
    in
	if n <= 1 orelse res = n then 1
	else res
    end
(*
val test_01 = largest_prime_factor 0 = 1
val test_02 = largest_prime_factor 1 = 1
val test_03 = largest_prime_factor 2 = 1
val test_04 = largest_prime_factor 3 = 1
val test_05 = largest_prime_factor 4 = 2
val test_06 = largest_prime_factor 5 = 1
val test_07 = largest_prime_factor 6 = 3
val test_08 = largest_prime_factor 7 = 1
val test_09 = largest_prime_factor 8 = 2
val test_10 = largest_prime_factor 9 = 3
val test_11 = largest_prime_factor 10 = 5
val test_12 = largest_prime_factor 11 = 1
val test_13 = largest_prime_factor 12 = 3
*)
val n = 600851475143 : Int64.int
val res1 = largest_prime_factor n
