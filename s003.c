#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
 * gcc s003.c -lm -o s003.out
 */

void aux(int *ptr, long long n)
{
    int i;
    int upper = (int) (sqrt(n) + 1);
    for (i = 2; i < upper; ++i) {
        if (n % i == 0) {
            *ptr = n / i;            
            aux(ptr, n / i);
            return;
        }
    }    
}
/*
 * Calculate the largest prime factor of n.
 * Return 1 if n is prime.
 */
int largest_prime_factor(long long n)
{
    int res = 1;
    aux(&res, n);
    return res;
    
}

void test()
{
    int pairs[16][2] = {
            {-5, 1}, {-4, 1}, {-3, 1}, {-2, 1}, {-1, 1}, {-0, 1}, {1, 1}, {2, 1},
            {3, 1}, {4, 2}, {5, 1}, {6, 3}, {7, 1}, {8, 2}, {9, 3}, {10, 5},
    };
    int i;
    for (i = 0; i < 16; ++i) {
        int res = largest_prime_factor((long long) pairs[i][0]);
        if (pairs[i][1] != res) {
            printf("Test of largestPrimeFactor(%d) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);
            return;
        }        
    }
    printf("All tests passed.\n");
}

int main(void)
{
/*
    test();
*/
    long long n = 600851475143LL;
    int res = largest_prime_factor(n);
    printf("%d\n", res);
    
    return 0;
}
