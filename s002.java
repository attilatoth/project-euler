interface Predicate<T> {
    boolean isTrue(T element);
}

public class s002 {

    /**
     * Predicate class that is used to check if an element is even.
     */
    private static class IsEven implements Predicate<Long> {
        @Override
        public boolean isTrue(Long i) {
            return i % 2 == 0;
        }
    }

    private static final double SQRT_5 = Math.pow(5, 0.5);
    private static final double PHI = (1 + SQRT_5) / 2;

    /**
     * Calculate the nth element of the Fibonacci squence
     * using the golden ratio.
     *
     * @param n index of the element of the sequence (n >= 0)
     * @return the nth element
     */
    private static long fibNthGoldenRatio(int n) {
        return n < 0 ? 0 : Math.round((Math.pow(PHI, n) - Math.pow(-PHI, -n)) / SQRT_5);
    }

    /**
     * Calculate the sum of elements in the Fibonacci sequence that
     * satisfy the predicate and less than or equal to upperLimit.
     *
     * @param pred the condition
     * @param upperLimit the last element of the sequence that is examined
     * @return the sum of the aforementioned sequence
     */
    private static long sumOfElements(int upperLimit, Predicate<Long> pred) {
        long a = 0;
        long b = 1;
        long sum = 0;        
        while (a <= upperLimit) {
            if (pred.isTrue(a)) {
                sum += a;
            }
            long tmp = b;
            b += a;
            a = tmp;
        }
        return sum;
    }

    /**
     * Calculate the sum of elements in the Fibonacci sequence that
     * satisfy the predicate and less than or equal to upperLimit.
     * The elements of the sequence if provided by {@link #fibNthGoldenRatio(int) fibNthGoldenRatio}.
     *
     * @param pred the condition
     * @param upperLimit the last element of the sequence that is examined
     * @return the sum of the aforementioned sequence
     */
    private static long sumOfElementsGR(int upperLimit, Predicate<Long> pred) {
        int n = 0;
        long sum = 0;
        long fibNth = 0;
        while (fibNth <= upperLimit) {
            if (pred.isTrue(fibNth)) {
                sum += fibNth;
            }
            fibNth = fibNthGoldenRatio(n);
            ++n;
        }
        return sum;
    }

    private static void test() {
        /*
        0 1 1 2 3 5 8 13 21
        */
        int[][] pairs = { {0, 0}, {1, 0}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {8, 10}, {9, 10} };

        Predicate<Long> pred = new IsEven();
        
        for (int i = 0; i < pairs.length; ++i) {
            long res = sumOfElements(pairs[i][0], pred);
            if (res != pairs[i][1]) {
                System.out.printf("Test of sumOfElements(%d, new IsEven()) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);            
               return;
            }
        }

        for (int i = 0; i < pairs.length; ++i) {
            long res = sumOfElementsGR(pairs[i][0], pred);
            if (res != pairs[i][1]) {
                System.out.printf("Test of sumOfElementsGR(%d, new IsEven()) failed.\n\tShould be %d but was %d.\n", pairs[i][0], pairs[i][1], res);            
               return;
            }
        }
        
        System.out.println("All tests passed.\n");
    }

    public static void main(String[] args) {

        // test();

        int upperLimit = 4 * 1000 * 1000;
        Predicate<Long> pred = new IsEven();
        
        // System.out.printf("sum=%d\n", sumOfElementsGR(upperLimit, pred));
        System.out.printf("sum=%d\n", sumOfElements(upperLimit, pred));
    }
}
