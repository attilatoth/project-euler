(*
 * Calculate the sum of the elements of the Fibonacci sequence that are 
 * in [0, limit] and satisfy the predicate.
 *)
fun sum_fibs_with_condition pred limit =
    let	
	fun aux a b acc =
	    if a <= limit andalso pred a then aux b (a + b) (acc + a)
	    else if a <= limit andalso not (pred a) then aux b (a + b) acc
	    else acc
    in
	aux 0 1 0
    end

fun is_even n = n mod 2 = 0
val even_fibs_upto = sum_fibs_with_condition is_even

(*
val test_01 = even_fibs_upto 0 = 0
val test_02 = even_fibs_upto 1 = 0
val test_03 = even_fibs_upto 2 = 2
val test_04 = even_fibs_upto 3 = 2
val test_05 = even_fibs_upto 4 = 2
val test_06 = even_fibs_upto 5 = 2
val test_07 = even_fibs_upto 8 = 10
val test_08 = even_fibs_upto 9 = 10
val test_09 = even_fibs_upto 10 = 10
val test_10 = even_fibs_upto 11 = 10
val test_11 = even_fibs_upto 12 = 10
val test_12 = even_fibs_upto 13 = 10
val test_13 = even_fibs_upto 21 = 10
*)

val limit = 4 * 1000 * 1000
val res = even_fibs_upto limit
