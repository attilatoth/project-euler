import java.util.Arrays;

public class s001 {    

    /**
     * Calculate the sum of integers in [1, upper_limit) that are multiples
     * of any of the integers given in conds.
     * 
     * @param upperBound the upper boundary of the interval
     * @param conds list of integers the sum of multiples of which to calculate
     * @return the sum of the aformentioned sequence
     */
    private static long calcSumOfMultiples(int upperBound, int... conds) {

        long sum = 0L;
        for (int i = 1; i < upperBound; ++i) {

            boolean isMultiple = false;
            int j = 0;
            while (!isMultiple && j < conds.length) {
                isMultiple |= i % conds[j] == 0;
                ++j;                
            }
            if (isMultiple) {
                sum += i;
            }
        }        
        return sum;
    }

    private static void test() {
        int[] ints = { 2, 3 };
        int[][] pairs = { 
            { 0, 0 }, { 2, 0 }, { 9, 23 }, { 10, 32 }, { 11, 42 }, { 12, 42 }, { 13, 54 }
        };
        for (int i = 0; i < pairs.length; ++i) {
            long res = calcSumOfMultiples(pairs[i][0], ints);
            if (res != pairs[i][1]) {
                System.out.printf("Test of calcSumOfMultiples(%d, %s) failed.\n\tShould be %d but was %d.\n",
                    pairs[i][0], Arrays.toString(ints), pairs[i][1], res);
                return;
            }
        }
        System.out.printf("All tests passed.\n");
    }

    public static void main(String[] args) {

        // test();

        long res = calcSumOfMultiples(1000, 3, 5);
        System.out.printf("sum=%d\n", res);
    }
}
