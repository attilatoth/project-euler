(* 
 * Calculate the sum of integers in [1, upper_limit)
 * that are multiples of either of the integers.
 *
 * a first divisor
 * b second divisor
 * n upper_limit (exclusive)
 *)
fun calc_sum_of_multiples a b n =
    let
	fun aux n acc =
	    if n <= 1 then acc
	    else if n mod a = 0 orelse n mod b = 0 then aux (n - 1) (n + acc)
	    else aux (n - 1) acc
    in
	aux (n - 1) 0
    end
(*
val test_01 = calc_sum_of_multiples 2 3 0 = 0
val test_02 = calc_sum_of_multiples 2 3 1 = 0
val test_03 = calc_sum_of_multiples 2 3 2 = 0
val test_04 = calc_sum_of_multiples 2 3 3 = 2
val test_05 = calc_sum_of_multiples 2 3 4 = 5
val test_06 = calc_sum_of_multiples 2 3 5 = 9
val test_07 = calc_sum_of_multiples 2 3 6 = 9
val test_08 = calc_sum_of_multiples 2 3 7 = 15
val test_09 = calc_sum_of_multiples 2 3 8 = 15
val test_10 = calc_sum_of_multiples 2 3 9 = 23
val test_11 = calc_sum_of_multiples 2 3 10 = 32
val test_12 = calc_sum_of_multiples 2 3 11 = 42
*)

val result = calc_sum_of_multiples 3 5 1000
