#!/usr/bin/env python

from math import sqrt
import unittest


def largest_prime_factor(n):
    """Calculate the largest prime factor of n.
    Return 1 if n is prime.
    """
    if n < 2:
        return 1

    res = [1]

    def aux(n):
        """The last number assigned to res[0] is the biggest factor"""
        # for x in xrange(2, n):
        for x in range(2, int(sqrt(n) + 1)):
            if n % x == 0:
                res[0] = n / x
                return aux(res[0])
    aux(n)
    return res[0]


class LargestPrimeFac0torTest(unittest.TestCase):

    def setUp(self):
        self.pairs = (
            (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (-0, 1), (1, 1), (
                2, 1), (3, 1), (4, 2), (5, 1), (6, 3), (7, 1), (8, 2), (9, 3), (10, 5)
        )

    def test(self):
        for pair in self.pairs:
            res = largest_prime_factor(pair[0])
            self.assertEqual(
                pair[1], res, "largest_prime_factor({}) should be {} but was {}.".format(pair[0], pair[1], res))


if __name__ == '__main__':

    # unittest.main()

    n = 600851475143
    print largest_prime_factor(n)
